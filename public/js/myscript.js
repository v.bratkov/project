$(document).ready(function () {
    $('.slick-slider1').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        prevArrow: $('.prev'),
        nextArrow: $('.next'),
        fade: true,
        responsive: [{
            breakpoint: 768,
            settings: {
                arrows: false,
                slidesToShow: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                arrows: false,
                slidesToShow: 1
            }
        }
        ]
    });
    var $status = $('.slide-num-current');
    var $slickElement = $('.slick-slider');
    $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (currentSlide ? currentSlide : 0) + 1;
        $status.text(`0${i}`);
    });
    $('.slick-slider2').slick({
        slidesToShow: 6,
        autoplay: true,
        autoplaySpeed: 2000,
        accessibility: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                arrows: false,
                slidesToShow: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                arrows: false,
                slidesToShow: 2
            }
        }
        ]
    });
    $('.slick-slider3').slick({
        slidesToShow: 4,
        autoplay: true,
        autoplaySpeed: 2500,
        accessibility: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                arrows: false,
                slidesToShow: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                arrows: false,
                slidesToShow: 2
            }
        }
        ]
    });
});
window.removeFakeCaptcha = function () {
    document.querySelector('.captcha-fake-field').remove();
}